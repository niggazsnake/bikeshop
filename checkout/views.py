# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import messages
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django import http
from django.utils.translation import ugettext as _

from oscar.apps.checkout.views import \
    IndexView as CoreIndexView, \
    ShippingMethodView as CoreShippingMethodView, \
    ShippingAddressView as CoreShippingAddressView, \
    PaymentDetailsView as CorePaymentDetailsView, \
    ThankYouView as CoreThankYouView, \
    UserAddress
from oscar.apps.shipping.methods import NoShippingRequired
from oscar.core.compat import user_is_authenticated
from shipping.methods import SelfPickup
from shipping.repository import Repository


class IndexView(CoreIndexView):
    success_url = reverse_lazy('checkout:shipping-method')


class ShippingMethodView(CoreShippingMethodView):
    def post(self, request, *args, **kwargs):
        method_code = request.POST.get('method_code', None)
        self.checkout_session.reset_shipping_data()
        self.checkout_session.use_shipping_method(method_code)

        if method_code != SelfPickup.code:
            return redirect('checkout:shipping-address')

        return redirect('checkout:payment-method')

    def get(self, request, *args, **kwargs):
        # These pre-conditions can't easily be factored out into the normal
        # pre-conditions as they do more than run a test and then raise an
        # exception on failure.

        # Check that shipping is required at all
        if not request.basket.is_shipping_required():
            # No shipping required - we store a special code to indicate so.
            self.checkout_session.use_shipping_method(
                NoShippingRequired().code)
            return self.get_success_response()

        # Check that shipping address has been completed
        # if not self.checkout_session.is_shipping_address_set():
        #     messages.error(request, _("Please choose a shipping address"))
        #     return redirect('checkout:shipping-address')

        # Save shipping methods as instance var as we need them both here
        # and when setting the context vars.
        self._methods = self.get_available_shipping_methods()
        if len(self._methods) == 0:
            # No shipping methods available for given address
            messages.warning(request, _(
                "Shipping is unavailable for your chosen address - please "
                "choose another"))
            return redirect('checkout:shipping-address')
        elif len(self._methods) == 1:
            # Only one shipping method - set this and redirect onto the next
            # step
            self.checkout_session.use_shipping_method(self._methods[0].code)
            return self.get_success_response()

        # Must be more than one available shipping method, we present them to
        # the user to make a choice.
        return super(CoreShippingMethodView, self).get(request, *args, **kwargs)


class ShippingAddressView(CoreShippingAddressView):
    success_url = reverse_lazy('checkout:payment-method')

    def post(self, request, *args, **kwargs):
        # Check if a shipping address was selected directly (eg no form was
        # filled in)
        if user_is_authenticated(self.request.user) \
                and 'address_id' in self.request.POST:
            address = UserAddress._default_manager.get(
                pk=self.request.POST['address_id'], user=self.request.user)
            action = self.request.POST.get('action', None)
            if action == 'ship_to':
                method_code = self.checkout_session.shipping_method_code(self.request.basket)
                # User has selected a previous address to ship to
                self.checkout_session.ship_to_user_address(address)
                if method_code:
                    self.checkout_session.use_shipping_method(method_code)
                return redirect(self.get_success_url())
            else:
                return http.HttpResponseBadRequest()
        else:
            return super(ShippingAddressView, self).post(
                request, *args, **kwargs)


class PaymentDetailsView(CorePaymentDetailsView):

    def get(self, request, *args, **kwargs):
        if not self.preview:
            return self.get_success_response()
        return super(PaymentDetailsView, self).get(request, *args, **kwargs)

    def get_success_response(self):
        return redirect('checkout:preview')


class ThankYouView(CoreThankYouView):
    def get_context_data(self, **kwargs):
        context = super(ThankYouView, self).get_context_data(**kwargs)
        context['order_shipping_method'] = Repository().get_shipping_method_by_code(self.object.shipping_code)
        return context
