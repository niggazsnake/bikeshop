from django import forms
from treebeard.forms import movenodeform_factory
from oscar.core.loading import get_model
from oscar.apps.dashboard.catalogue.forms import ProductForm as CoreProductForm


Category = get_model('catalogue', 'Category')
Product = get_model('catalogue', 'Product')

CategoryForm = movenodeform_factory(
    Category,
    fields=['name', 'description', 'image', 'product_class'])


class ProductForm(CoreProductForm):
    class Meta(CoreProductForm.Meta):
        fields = CoreProductForm.Meta.fields + ['short_description']
