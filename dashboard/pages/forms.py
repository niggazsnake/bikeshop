# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django import forms
from oscar.apps.dashboard.pages import forms as pages_forms
from redactor.widgets import RedactorEditor


class PageUpdateForm(pages_forms.PageUpdateForm):
    class Meta(pages_forms.PageUpdateForm.Meta):
        widgets = {
            'content': RedactorEditor()
        }

    class Media:
        css = {
            'all': ('dashboard/redactor.css',)
        }
