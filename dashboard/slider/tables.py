# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ungettext_lazy

from django_tables2 import Table, TemplateColumn
from oscar.core.loading import get_class
from slider.models import Slide

DashboardTable = get_class('dashboard.tables', 'DashboardTable')


class SliderTable(Table):
    class Meta(DashboardTable.Meta):
        model = Slide

    actions = TemplateColumn(
        template_name='dashboard/slider/slide_row_actions.html',
        orderable=False
    )

    icon = "sitemap"
    caption = ungettext_lazy("%s Slide", "%s Slides")
