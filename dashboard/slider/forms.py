# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms
from treebeard.forms import movenodeform_factory
from slider.models import Slide


# class SlideForm(forms.ModelForm):
#     class Meta:
#         model = Slide
#         fields = ['image', 'title']
SlideForm = movenodeform_factory(
    Slide,
    fields=['image', 'title']
)
