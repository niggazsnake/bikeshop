# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, TemplateView, DeleteView, UpdateView
from django_tables2 import SingleTableMixin
from slider.models import Slide
from .forms import SlideForm
from .tables import SliderTable


class SlideListView(SingleTableMixin, TemplateView):
    template_name = 'dashboard/slider/slide_list.html'
    context_table_name = 'slides'
    table_class = SliderTable
    table_data = Slide.objects.all()


class SlideCreateView(CreateView):
    model = Slide
    template_name = 'dashboard/slider/slide_form.html'
    form = SlideForm
    fields = '__all__'
    success_url = reverse_lazy('dashboard:slide-list')

    def get_context_data(self, **kwargs):
        ctx = super(SlideCreateView, self).get_context_data(**kwargs)
        ctx['title'] = 'Добавить слайд'
        return ctx


class SlideUpdateView(UpdateView):
    model = Slide
    template_name = 'dashboard/slider/slide_form.html'
    form = SlideForm
    fields = '__all__'
    success_url = reverse_lazy('dashboard:slide-list')

    def get_context_data(self, **kwargs):
        ctx = super(SlideUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = 'Изменить слайд %s' % self.object.title
        return ctx


class SlideDeleteView(DeleteView):
    model = Slide
    template_name = 'dashboard/slider/slide_delete.html'
    success_url = reverse_lazy('dashboard:slide-list')

    def get_context_data(self, **kwargs):
        ctx = super(SlideDeleteView, self).get_context_data(**kwargs)
        ctx['title'] = 'Удалить слайд %s' % self.object.title
        return ctx
