# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url
from oscar.core.application import DashboardApplication
from oscar.core.loading import get_class


class SliderDashboardApplication(DashboardApplication):
    name = None
    default_permissions = ['is_staff', ]

    slide_list_view = get_class('dashboard.slider.views', 'SlideListView')
    slide_create_view = get_class('dashboard.slider.views', 'SlideCreateView')
    slide_update_view = get_class('dashboard.slider.views', 'SlideUpdateView')
    slide_delete_view = get_class('dashboard.slider.views', 'SlideDeleteView')

    def get_urls(self):
        urls = [
            url(r'^list/$', self.slide_list_view.as_view(), name='slide-list'),
            url(r'^add/$', self.slide_create_view.as_view(), name='slide-add'),
            url(r'^(?P<pk>\d+)/delete/$', self.slide_delete_view.as_view(), name='slide-delete'),
            url(r'^(?P<pk>\d+)/update/$', self.slide_update_view.as_view(), name='slide-update'),
        ]
        return self.post_process_urls(urls)


application = SliderDashboardApplication()
