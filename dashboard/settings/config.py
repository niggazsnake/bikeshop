# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class SettingsDashboardConfig(AppConfig):
    label = 'settings_dashboard'
    name = 'dashboard.settings'
    verbose_name = 'Настройки'
