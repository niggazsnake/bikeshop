# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django import forms
from redactor.widgets import RedactorEditor
from articles.models import Article


class ArticleUpdateForm(forms.ModelForm):
    class Meta:
        model = Article
        exclude = ['views']
        widgets = {
            'text': RedactorEditor()
        }

    class Media:
        css = {
            'all': ('dashboard/redactor.css',)
        }
