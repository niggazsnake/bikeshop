# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from oscar.apps.catalogue.search_handlers import SimpleProductSearchHandler as CoreSimpleProductSearchHandler
from .filters import ProductFilter


class SimpleProductSearchHandler(CoreSimpleProductSearchHandler):
    def __init__(self, request_data, full_path, categories=None, category=None):
        product_class = None
        if category:
            product_class = category.product_class
        super(SimpleProductSearchHandler, self).__init__(request_data, full_path, categories)
        self.filter = ProductFilter(request_data, queryset=self.object_list, product_class=product_class)
        self.object_list = self.filter.qs

    def get_search_context_data(self, context_object_name):
        context = super(SimpleProductSearchHandler, self).get_search_context_data(context_object_name)
        context['filter'] = self.filter
        return context
