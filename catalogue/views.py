# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from oscar.apps.catalogue.views import ProductCategoryView as CoreProductCategoryView


class ProductCategoryView(CoreProductCategoryView):
    def get_search_handler(self, *args, **kwargs):
        kwargs['category'] = self.category
        return super(ProductCategoryView, self).get_search_handler(*args, **kwargs)

    def get_template_names(self):
        template_names = super(ProductCategoryView, self).get_template_names()
        if self.category.has_children():
            return ['catalogue/category_with_sub_categories.html']
        return template_names
