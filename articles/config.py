# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class ArticlesConfig(AppConfig):
    label = 'articles'
    name = 'articles'
    verbose_name = 'Статьи'
