# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.contrib.sitemaps import Sitemap
from .models import Article


class ArticleSitemap(Sitemap):
    def items(self):
        return Article.objects.filter(is_active=True)

    def lastmod(self, obj):
        return obj.date_update
