# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views import generic
from articles.models import Article


class ArticleDetailView(generic.DetailView):
    queryset = Article.objects.filter(is_active=True)


class ArticleListView(generic.ListView):
    queryset = Article.objects.filter(is_active=True)
    paginate_by = 20
