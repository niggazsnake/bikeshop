# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf.urls import url
from articles.views import ArticleDetailView, ArticleListView

urlpatterns = [
    url(r'^$', ArticleListView.as_view(), name='article-list'),
    url(r'^(?P<slug>[\w-]+)/$', ArticleDetailView.as_view(), name='article-detail'),
]
