# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
THUMBNAIL_DEBUG = DEBUG

for template_engine in TEMPLATES:
    template_engine['OPTIONS']['debug'] = DEBUG

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*l@x$oe(*^+hzkb*$vgf8!*o6x4o-g@@c$ex5-ar_(ue_+t$sd'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

try:
    from .local import *
except ImportError:
    pass
