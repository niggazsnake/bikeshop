# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from oscar.apps.shipping import repository
from . import methods


class Repository(repository.Repository):
    methods = [
        methods.SelfPickup(),
        methods.Courier(),
    ]

    def get_shipping_method_by_code(self, code):
        for method in self.methods:
            if code == method.code:
                return method
