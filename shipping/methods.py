# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from decimal import Decimal as D

from constance import config
from oscar.apps.shipping import methods


class SelfPickup(methods.Free):
    code = 'self-pickup'
    name = 'Самовывоз'

    @property
    def description(self):
        return 'Вы самостоятельно можете забрать Ваш заказ из нашего офиса, расположенного по адресу:<br>{0}'.format(
            config.PICKUP_ADDRESS)


class Courier(methods.FixedPrice):
    code = 'courier'
    name = 'Доставка курьером'
    description = 'Доставка в удобное для вас место и время. Осмотреть товар и ' \
                  'принять решение можно до оплаты. Если товар не подошел, платить не нужно.'

    charge_excl_tax = D('100.00')
    charge_incl_tax = D('100.00')
