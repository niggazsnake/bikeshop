# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.db import models
from django.utils.six import python_2_unicode_compatible
from oscar.core.loading import get_model
from sorl.thumbnail import ImageField

Category = get_model('catalogue', 'category')
Product = get_model('catalogue', 'product')


@python_2_unicode_compatible
class Slide(models.Model):
    image = ImageField('Изображение слайда', upload_to="images/slider/", max_length=255)
    title = models.CharField('Заголовок слайда', max_length=255)
    to_category = models.ForeignKey(Category, verbose_name='Ссылка на категорию', blank=True, null=True)
    to_product = models.ForeignKey(Product, verbose_name='Ссылка на товар', blank=True, null=True)

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды'

    @property
    def link(self):
        if self.to_product:
            return self.to_product
        elif self.to_category:
            return self.to_category
        else:
            return None

    def __str__(self):
        return "%s : %s" % (self.title, self.to_category)
